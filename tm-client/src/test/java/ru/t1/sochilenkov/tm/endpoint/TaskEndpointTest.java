package ru.t1.sochilenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.sochilenkov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.sochilenkov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.dto.request.*;
import ru.t1.sochilenkov.tm.enumerated.Status;
import ru.t1.sochilenkov.tm.marker.SoapCategory;
import ru.t1.sochilenkov.tm.model.Task;
import ru.t1.sochilenkov.tm.service.PropertyService;

import java.util.List;
import java.util.UUID;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @Nullable
    private String firstToken;

    @Nullable
    private String secondToken;

    @Nullable
    private Task task;

    @Before
    @SneakyThrows
    public void init() {
        firstToken = authEndpoint.login(new UserLoginRequest("user 1", "1")).getToken();
        secondToken = authEndpoint.login(new UserLoginRequest("user 2", "2")).getToken();
        @NotNull TaskCreateRequest taskCreateRequest = new TaskCreateRequest(firstToken);
        taskCreateRequest.setName("initial_proj_1");
        taskCreateRequest.setDescription("initial_desc_1");
        taskEndpoint.createTask(taskCreateRequest);
        taskCreateRequest = new TaskCreateRequest(secondToken);
        taskCreateRequest.setName("initial_proj_2");
        taskCreateRequest.setDescription("initial_desc_2");
        taskEndpoint.createTask(taskCreateRequest);
        @NotNull ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(firstToken);
        projectCreateRequest.setName("initial_proj_1");
        projectCreateRequest.setDescription("initial_desc_1");
        projectEndpoint.createProject(projectCreateRequest);
        @NotNull TaskBindToProjectRequest taskBindToProjectRequest = new TaskBindToProjectRequest(firstToken);
        taskBindToProjectRequest.setTaskId(
                taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getId()
        );
        taskBindToProjectRequest.setProjectId(
                projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getId()
        );
        taskEndpoint.bindTaskToProject(taskBindToProjectRequest);
        projectCreateRequest.setToken(secondToken);
        projectCreateRequest.setName("initial_proj_2");
        projectCreateRequest.setDescription("initial_desc_2");
        projectEndpoint.createProject(projectCreateRequest);
    }

    @After
    @SneakyThrows
    public void end() {
        projectEndpoint.clearProject(new ProjectClearRequest(firstToken));
        projectEndpoint.clearProject(new ProjectClearRequest(secondToken));
        taskEndpoint.clearTask(new TaskClearRequest(firstToken));
        taskEndpoint.clearTask(new TaskClearRequest(secondToken));
        authEndpoint.logout(new UserLogoutRequest(firstToken));
        authEndpoint.logout(new UserLogoutRequest(secondToken));
    }

    @Test
    public void testTaskCreateNegative() {
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.createTask(new TaskCreateRequest()));
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.createTask(new TaskCreateRequest(firstToken)));
    }

    @Test
    @SneakyThrows
    public void testTaskCreatePositive() {
        @NotNull TaskCreateRequest taskCreateRequest = new TaskCreateRequest(firstToken);
        taskCreateRequest.setName("test_proj_1");
        taskCreateRequest.setDescription("test_desc_1");
        taskEndpoint.createTask(taskCreateRequest).getTask();
        Assert.assertEquals(2, taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().size());
        taskCreateRequest = new TaskCreateRequest(secondToken);
        taskCreateRequest.setName("test_proj_2");
        taskCreateRequest.setDescription("test_desc_2");
        taskEndpoint.createTask(taskCreateRequest).getTask();
        Assert.assertEquals(2, taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().size());
    }

    @Test
    public void testTaskListNegative() {
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.listTask(new TaskListRequest()));
    }

    @Test
    public void testTaskListPositive() {
        Assert.assertEquals(1, taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().size());
        Assert.assertEquals(
                "initial_proj_1",
                taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getName()
        );
        Assert.assertEquals(1, taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().size());
        Assert.assertEquals(
                "initial_proj_2",
                taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getName()
        );
    }

    @Test
    public void testTaskRemoveByIdNegative() {
        TaskRemoveByIdRequest taskRemoveByIdRequest = new TaskRemoveByIdRequest();
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.removeTaskById(taskRemoveByIdRequest));
        taskRemoveByIdRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.removeTaskById(taskRemoveByIdRequest));
        taskRemoveByIdRequest.setId("");
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(firstToken)));
    }

    @Test
    @SneakyThrows
    public void testTaskRemoveByIdPositive() {
        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks());
        @NotNull String taskId = taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getId();
        Assert.assertEquals(1, taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().size());
        @NotNull TaskRemoveByIdRequest taskRemoveByIdRequest = new TaskRemoveByIdRequest(firstToken);
        taskRemoveByIdRequest.setId(taskId);
        Assert.assertNotNull(taskEndpoint.removeTaskById(taskRemoveByIdRequest));
        Assert.assertNull(taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks());

        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks());
        taskId = taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getId();
        Assert.assertEquals(1, taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().size());
        taskRemoveByIdRequest = new TaskRemoveByIdRequest(secondToken);
        taskRemoveByIdRequest.setId(taskId);
        Assert.assertNotNull(taskEndpoint.removeTaskById(taskRemoveByIdRequest));
        Assert.assertNull(taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks());
    }

    @Test
    public void testTaskRemoveByIndexNegative() {
        TaskRemoveByIndexRequest taskRemoveByIndexRequest = new TaskRemoveByIndexRequest();
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.removeTaskByIndex(taskRemoveByIndexRequest));
        taskRemoveByIndexRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.removeTaskByIndex(taskRemoveByIndexRequest));
    }

    @Test
    @SneakyThrows
    public void testTaskRemoveByIndexPositive() {
        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks());
        Assert.assertEquals(1, taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().size());
        @NotNull TaskRemoveByIndexRequest taskRemoveByIndexRequest = new TaskRemoveByIndexRequest(firstToken);
        taskRemoveByIndexRequest.setIndex(0);
        Assert.assertNotNull(taskEndpoint.removeTaskByIndex(taskRemoveByIndexRequest));
        Assert.assertNull(taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks());

        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks());
        Assert.assertEquals(1, taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().size());
        taskRemoveByIndexRequest = new TaskRemoveByIndexRequest(secondToken);
        taskRemoveByIndexRequest.setIndex(0);
        Assert.assertNotNull(taskEndpoint.removeTaskByIndex(taskRemoveByIndexRequest));
        Assert.assertNull(taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks());
    }

    @Test
    public void testTaskCompleteByIdNegative() {
        TaskCompleteByIdRequest taskCompleteByIdRequest = new TaskCompleteByIdRequest();
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.completeTaskById(taskCompleteByIdRequest));
        taskCompleteByIdRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.completeTaskById(taskCompleteByIdRequest));
        taskCompleteByIdRequest.setId("");
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.completeTaskById(taskCompleteByIdRequest));
        taskCompleteByIdRequest.setId(UUID.randomUUID().toString());
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.completeTaskById(taskCompleteByIdRequest));
    }

    @Test
    public void testTaskCompleteByIdPositive() {
        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks());
        Assert.assertEquals(Status.NOT_STARTED, taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getStatus());
        @NotNull String taskId = taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getId();
        @NotNull TaskCompleteByIdRequest taskCompleteByIdRequest = new TaskCompleteByIdRequest(firstToken);
        taskCompleteByIdRequest.setId(taskId);
        Assert.assertNotNull(taskEndpoint.completeTaskById(taskCompleteByIdRequest));
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getStatus());

        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks());
        Assert.assertEquals(Status.NOT_STARTED, taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getStatus());
        taskId = taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getId();
        taskCompleteByIdRequest = new TaskCompleteByIdRequest(secondToken);
        taskCompleteByIdRequest.setId(taskId);
        Assert.assertNotNull(taskEndpoint.completeTaskById(taskCompleteByIdRequest));
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getStatus());
    }

    @Test
    public void testTaskCompleteByIndexNegative() {
        TaskCompleteByIndexRequest taskCompleteByIndexRequest = new TaskCompleteByIndexRequest();
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.completeTaskByIndex(taskCompleteByIndexRequest));
        taskCompleteByIndexRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.completeTaskByIndex(taskCompleteByIndexRequest));
        taskCompleteByIndexRequest.setIndex(-1);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.completeTaskByIndex(taskCompleteByIndexRequest));
        taskCompleteByIndexRequest.setIndex(9999);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.completeTaskByIndex(taskCompleteByIndexRequest));
    }

    @Test
    public void testTaskCompleteByIndexPositive() {
        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks());
        Assert.assertEquals(Status.NOT_STARTED, taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getStatus());
        @NotNull TaskCompleteByIndexRequest taskCompleteByIndexRequest = new TaskCompleteByIndexRequest(firstToken);
        taskCompleteByIndexRequest.setIndex(0);
        taskEndpoint.completeTaskByIndex(taskCompleteByIndexRequest);
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getStatus());

        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks());
        Assert.assertEquals(Status.NOT_STARTED, taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getStatus());
        taskCompleteByIndexRequest = new TaskCompleteByIndexRequest(secondToken);
        taskCompleteByIndexRequest.setIndex(0);
        taskEndpoint.completeTaskByIndex(taskCompleteByIndexRequest);
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getStatus());
    }

    @Test
    public void testTaskStartByIdNegative() {
        TaskStartByIdRequest taskStartByIdRequest = new TaskStartByIdRequest();
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.startTaskById(taskStartByIdRequest));
        taskStartByIdRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.startTaskById(taskStartByIdRequest));
        taskStartByIdRequest.setId("");
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.startTaskById(taskStartByIdRequest));
        taskStartByIdRequest.setId(UUID.randomUUID().toString());
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.startTaskById(taskStartByIdRequest));
    }

    @Test
    public void testTaskStartByIdPositive() {
        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks());
        Assert.assertEquals(Status.NOT_STARTED, taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getStatus());
        @NotNull String taskId = taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getId();
        @NotNull TaskStartByIdRequest taskStartByIdRequest = new TaskStartByIdRequest(firstToken);
        taskStartByIdRequest.setId(taskId);
        Assert.assertNotNull(taskEndpoint.startTaskById(taskStartByIdRequest));
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getStatus());

        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks());
        Assert.assertEquals(Status.NOT_STARTED, taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getStatus());
        taskId = taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getId();
        taskStartByIdRequest = new TaskStartByIdRequest(secondToken);
        taskStartByIdRequest.setId(taskId);
        Assert.assertNotNull(taskEndpoint.startTaskById(taskStartByIdRequest));
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getStatus());
    }

    @Test
    public void testTaskStartByIndexNegative() {
        TaskStartByIndexRequest taskStartByIndexRequest = new TaskStartByIndexRequest();
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.startTaskByIndex(taskStartByIndexRequest));
        taskStartByIndexRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.startTaskByIndex(taskStartByIndexRequest));
        taskStartByIndexRequest.setIndex(-1);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.startTaskByIndex(taskStartByIndexRequest));
        taskStartByIndexRequest.setIndex(9999);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.startTaskByIndex(taskStartByIndexRequest));
    }

    @Test
    public void testTaskStartByIndexPositive() {
        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks());
        Assert.assertEquals(Status.NOT_STARTED, taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getStatus());
        @NotNull TaskStartByIndexRequest taskStartByIndexRequest = new TaskStartByIndexRequest(firstToken);
        taskStartByIndexRequest.setIndex(0);
        taskEndpoint.startTaskByIndex(taskStartByIndexRequest);
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getStatus());

        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks());
        Assert.assertEquals(Status.NOT_STARTED, taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getStatus());
        taskStartByIndexRequest = new TaskStartByIndexRequest(secondToken);
        taskStartByIndexRequest.setIndex(0);
        taskEndpoint.startTaskByIndex(taskStartByIndexRequest);
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getStatus());
    }

    @Test
    public void testTaskChangeStatusByIdNegative() {
        TaskChangeStatusByIdRequest taskChangeStatusByIdRequest = new TaskChangeStatusByIdRequest();
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.changeTaskStatusById(taskChangeStatusByIdRequest));
        taskChangeStatusByIdRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.changeTaskStatusById(taskChangeStatusByIdRequest));
        taskChangeStatusByIdRequest.setId("");
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.changeTaskStatusById(taskChangeStatusByIdRequest));
        taskChangeStatusByIdRequest.setId(UUID.randomUUID().toString());
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.changeTaskStatusById(taskChangeStatusByIdRequest));
        taskChangeStatusByIdRequest.setId(taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getId());
        taskChangeStatusByIdRequest.setStatus(null);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.changeTaskStatusById(taskChangeStatusByIdRequest));
    }

    @Test
    public void testTaskChangeStatusByIdPositive() {
        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks());
        Assert.assertEquals(Status.NOT_STARTED, taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getStatus());
        @NotNull String taskId = taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getId();
        @NotNull TaskChangeStatusByIdRequest taskChangeStatusByIdRequest = new TaskChangeStatusByIdRequest(firstToken);
        taskChangeStatusByIdRequest.setId(taskId);
        taskChangeStatusByIdRequest.setStatus(Status.IN_PROGRESS);
        Assert.assertNotNull(taskEndpoint.changeTaskStatusById(taskChangeStatusByIdRequest));
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getStatus());

        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks());
        Assert.assertEquals(Status.NOT_STARTED, taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getStatus());
        taskId = taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getId();
        taskChangeStatusByIdRequest = new TaskChangeStatusByIdRequest(secondToken);
        taskChangeStatusByIdRequest.setId(taskId);
        taskChangeStatusByIdRequest.setStatus(Status.IN_PROGRESS);
        Assert.assertNotNull(taskEndpoint.changeTaskStatusById(taskChangeStatusByIdRequest));
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getStatus());
    }

    @Test
    public void testTaskChangeStatusByIndexNegative() {
        TaskChangeStatusByIndexRequest taskChangeStatusByIndexRequest = new TaskChangeStatusByIndexRequest();
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.changeTaskStatusByIndex(taskChangeStatusByIndexRequest));
        taskChangeStatusByIndexRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.changeTaskStatusByIndex(taskChangeStatusByIndexRequest));
        taskChangeStatusByIndexRequest.setIndex(-1);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.changeTaskStatusByIndex(taskChangeStatusByIndexRequest));
        taskChangeStatusByIndexRequest.setIndex(9999);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.changeTaskStatusByIndex(taskChangeStatusByIndexRequest));
        taskChangeStatusByIndexRequest.setIndex(1);
        taskChangeStatusByIndexRequest.setStatus(null);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.changeTaskStatusByIndex(taskChangeStatusByIndexRequest));
    }

    @Test
    public void testTaskChangeStatusByIndexPositive() {
        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks());
        Assert.assertEquals(Status.NOT_STARTED, taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getStatus());
        @NotNull TaskChangeStatusByIndexRequest taskChangeStatusByIndexRequest = new TaskChangeStatusByIndexRequest(firstToken);
        taskChangeStatusByIndexRequest.setIndex(0);
        taskChangeStatusByIndexRequest.setStatus(Status.IN_PROGRESS);
        Assert.assertNotNull(taskEndpoint.changeTaskStatusByIndex(taskChangeStatusByIndexRequest));
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getStatus());

        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks());
        Assert.assertEquals(Status.NOT_STARTED, taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getStatus());
        taskChangeStatusByIndexRequest = new TaskChangeStatusByIndexRequest(secondToken);
        taskChangeStatusByIndexRequest.setIndex(0);
        taskChangeStatusByIndexRequest.setStatus(Status.IN_PROGRESS);
        Assert.assertNotNull(taskEndpoint.changeTaskStatusByIndex(taskChangeStatusByIndexRequest));
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getStatus());
    }

    @Test
    public void testTaskShowByIdNegative() {
        TaskShowByIdRequest taskChangeStatusByIdRequest = new TaskShowByIdRequest();
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.showTaskById(taskChangeStatusByIdRequest));
        taskChangeStatusByIdRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.showTaskById(taskChangeStatusByIdRequest));
        taskChangeStatusByIdRequest.setId("");
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.showTaskById(taskChangeStatusByIdRequest));
    }

    @Test
    @SneakyThrows
    public void testTaskShowByIdPositive() {
        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks());
        @NotNull String taskId = taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getId();
        @NotNull TaskShowByIdRequest taskShowByIdRequest = new TaskShowByIdRequest(firstToken);
        taskShowByIdRequest.setId(taskId);
        Assert.assertNotNull(taskEndpoint.showTaskById(taskShowByIdRequest));
        Assert.assertEquals("initial_proj_1", taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getName());

        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks());
        taskId = taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getId();
        taskShowByIdRequest = new TaskShowByIdRequest(secondToken);
        taskShowByIdRequest.setId(taskId);
        Assert.assertNotNull(taskEndpoint.showTaskById(taskShowByIdRequest));
        Assert.assertEquals("initial_proj_2", taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getName());
    }

    @Test
    public void testTaskShowByIndexNegative() {
        TaskShowByIndexRequest taskChangeStatusByIndexRequest = new TaskShowByIndexRequest();
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.showTaskByIndex(taskChangeStatusByIndexRequest));
        taskChangeStatusByIndexRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.showTaskByIndex(taskChangeStatusByIndexRequest));
        taskChangeStatusByIndexRequest.setIndex(-1);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.showTaskByIndex(taskChangeStatusByIndexRequest));
    }

    @Test
    @SneakyThrows
    public void testTaskShowByIndexPositive() {
        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks());
        @NotNull TaskShowByIndexRequest taskShowByIndexRequest = new TaskShowByIndexRequest(firstToken);
        taskShowByIndexRequest.setIndex(1);
        Assert.assertNotNull(taskEndpoint.showTaskByIndex(taskShowByIndexRequest));
        Assert.assertEquals("initial_proj_1", taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getName());

        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks());
        taskShowByIndexRequest = new TaskShowByIndexRequest(secondToken);
        taskShowByIndexRequest.setIndex(1);
        Assert.assertNotNull(taskEndpoint.showTaskByIndex(taskShowByIndexRequest));
        Assert.assertEquals("initial_proj_2", taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getName());
    }

    @Test
    public void testTaskUpdateByIdNegative() {
        TaskUpdateByIdRequest taskUpdateByIdRequest = new TaskUpdateByIdRequest();
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest));
        taskUpdateByIdRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest));
        taskUpdateByIdRequest.setId("");
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest));
        taskUpdateByIdRequest.setId(UUID.randomUUID().toString());
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest));
        taskUpdateByIdRequest.setName("updated_init_proj_1");
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest));
    }

    @Test
    public void testTaskUpdateByIdPositive() {
        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks());
        @NotNull String taskId = taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getId();
        @NotNull TaskUpdateByIdRequest taskUpdateByIdRequest = new TaskUpdateByIdRequest(firstToken);
        taskUpdateByIdRequest.setId(taskId);
        taskUpdateByIdRequest.setName("updated_init_proj_1");
        taskUpdateByIdRequest.setDescription("updated_desc_proj_1");
        Assert.assertNotNull(taskEndpoint.updateTaskById(taskUpdateByIdRequest));
        Assert.assertEquals("updated_init_proj_1", taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getName());
        Assert.assertEquals("updated_desc_proj_1", taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getDescription());

        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks());
        taskId = taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getId();
        taskUpdateByIdRequest = new TaskUpdateByIdRequest(secondToken);
        taskUpdateByIdRequest.setId(taskId);
        taskUpdateByIdRequest.setName("updated_init_proj_2");
        taskUpdateByIdRequest.setDescription("updated_desc_proj_2");
        Assert.assertNotNull(taskEndpoint.updateTaskById(taskUpdateByIdRequest));
        Assert.assertEquals("updated_init_proj_2", taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getName());
        Assert.assertEquals("updated_desc_proj_2", taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getDescription());
    }

    @Test
    public void testTaskUpdateByIndexNegative() {
        TaskUpdateByIndexRequest taskUpdateByIndexRequest = new TaskUpdateByIndexRequest();
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.updateTaskByIndex(taskUpdateByIndexRequest));
        taskUpdateByIndexRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.updateTaskByIndex(taskUpdateByIndexRequest));
        taskUpdateByIndexRequest.setIndex(-1);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.updateTaskByIndex(taskUpdateByIndexRequest));
        taskUpdateByIndexRequest.setIndex(9999);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.updateTaskByIndex(taskUpdateByIndexRequest));
        taskUpdateByIndexRequest.setName("updated_init_proj_1");
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.updateTaskByIndex(taskUpdateByIndexRequest));
    }

    @Test
    public void testTaskUpdateByIndexPositive() {
        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks());
        @NotNull TaskUpdateByIndexRequest taskUpdateByIndexRequest = new TaskUpdateByIndexRequest(firstToken);
        taskUpdateByIndexRequest.setIndex(0);
        taskUpdateByIndexRequest.setName("updated_init_proj_1");
        taskUpdateByIndexRequest.setDescription("updated_desc_proj_1");
        taskEndpoint.updateTaskByIndex(taskUpdateByIndexRequest);
        Assert.assertEquals("updated_init_proj_1", taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getName());
        Assert.assertEquals("updated_desc_proj_1", taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getDescription());

        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks());
        taskUpdateByIndexRequest = new TaskUpdateByIndexRequest(secondToken);
        taskUpdateByIndexRequest.setIndex(0);
        taskUpdateByIndexRequest.setName("updated_init_proj_2");
        taskUpdateByIndexRequest.setDescription("updated_desc_proj_2");
        taskEndpoint.updateTaskByIndex(taskUpdateByIndexRequest);
        Assert.assertEquals("updated_init_proj_2", taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getName());
        Assert.assertEquals("updated_desc_proj_2", taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getDescription());
    }

    @Test
    @SneakyThrows
    public void testTaskClear() {
        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks());
        @NotNull TaskClearRequest taskClearRequest = new TaskClearRequest(firstToken);
        Assert.assertNotNull(taskEndpoint.clearTask(taskClearRequest));
        Assert.assertNull(taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks());

        Assert.assertNotNull(taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks());
        taskClearRequest = new TaskClearRequest(secondToken);
        Assert.assertNotNull(taskEndpoint.clearTask(taskClearRequest));
        Assert.assertNull(taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks());
    }

    @Test
    public void testTaskListByProjectIdPositive() {
        @NotNull TaskListByProjectIdRequest taskListByProjectIdRequest = new TaskListByProjectIdRequest(firstToken);
        taskListByProjectIdRequest.setProjectId(projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getId());
        @NotNull List<Task> tasks = taskEndpoint.listTaskByProjectId(taskListByProjectIdRequest).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        Assert.assertEquals("initial_proj_1", tasks.get(0).getName());
    }

    @Test
    public void testTaskBindToProjectNegative() {
        TaskBindToProjectRequest taskBindToProjectRequest = new TaskBindToProjectRequest();
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.bindTaskToProject(taskBindToProjectRequest));
        taskBindToProjectRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.bindTaskToProject(taskBindToProjectRequest));
        taskBindToProjectRequest.setProjectId("");
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.bindTaskToProject(taskBindToProjectRequest));
        taskBindToProjectRequest.setProjectId(UUID.randomUUID().toString());
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.bindTaskToProject(taskBindToProjectRequest));
        taskBindToProjectRequest.setTaskId("");
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.bindTaskToProject(taskBindToProjectRequest));
        taskBindToProjectRequest.setTaskId(UUID.randomUUID().toString());
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.bindTaskToProject(taskBindToProjectRequest));
        taskBindToProjectRequest.setTaskId(
                projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getId()
        );
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.bindTaskToProject(taskBindToProjectRequest));
    }

    @Test
    @SneakyThrows
    public void testTaskBindFromProjectPositive() {
        @NotNull TaskListByProjectIdRequest taskListByProjectIdRequest = new TaskListByProjectIdRequest(secondToken);
        @NotNull String projectId = projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getId();
        taskListByProjectIdRequest.setProjectId(projectId);
        @Nullable List<Task> tasks = taskEndpoint.listTaskByProjectId(taskListByProjectIdRequest).getTasks();
        Assert.assertNull(tasks);
        @NotNull TaskBindToProjectRequest taskBindToProjectRequest = new TaskBindToProjectRequest(secondToken);
        taskBindToProjectRequest.setTaskId(
                taskEndpoint.listTask(new TaskListRequest(secondToken)).getTasks().get(0).getId()
        );
        taskBindToProjectRequest.setProjectId(projectId);
        taskEndpoint.bindTaskToProject(taskBindToProjectRequest);
        tasks = taskEndpoint.listTaskByProjectId(taskListByProjectIdRequest).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        Assert.assertEquals("initial_proj_2", tasks.get(0).getName());
    }

    @Test
    public void testTaskUnbindFromProjectNegative() {
        TaskUnbindFromProjectRequest taskUnbindFromProjectRequest = new TaskUnbindFromProjectRequest();
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.unbindTaskFromProject(taskUnbindFromProjectRequest));
        taskUnbindFromProjectRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.unbindTaskFromProject(taskUnbindFromProjectRequest));
        taskUnbindFromProjectRequest.setProjectId("");
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.unbindTaskFromProject(taskUnbindFromProjectRequest));
        taskUnbindFromProjectRequest.setProjectId(UUID.randomUUID().toString());
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.unbindTaskFromProject(taskUnbindFromProjectRequest));
        taskUnbindFromProjectRequest.setTaskId("");
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.unbindTaskFromProject(taskUnbindFromProjectRequest));
        taskUnbindFromProjectRequest.setTaskId(UUID.randomUUID().toString());
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.unbindTaskFromProject(taskUnbindFromProjectRequest));
        taskUnbindFromProjectRequest.setTaskId(
                projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getId()
        );
        Assert.assertThrows(RuntimeException.class, () -> taskEndpoint.unbindTaskFromProject(taskUnbindFromProjectRequest));
    }

    @Test
    @SneakyThrows
    public void testTaskUnbindFromProjectPositive() {
        @NotNull TaskListByProjectIdRequest taskListByProjectIdRequest = new TaskListByProjectIdRequest(firstToken);
        @NotNull String projectId = projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getId();
        taskListByProjectIdRequest.setProjectId(projectId);
        @Nullable List<Task> tasks = taskEndpoint.listTaskByProjectId(taskListByProjectIdRequest).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        Assert.assertEquals("initial_proj_1", tasks.get(0).getName());
        @NotNull TaskUnbindFromProjectRequest taskUnbindFromProjectRequest = new TaskUnbindFromProjectRequest(firstToken);
        taskUnbindFromProjectRequest.setTaskId(
                taskEndpoint.listTask(new TaskListRequest(firstToken)).getTasks().get(0).getId()
        );
        taskUnbindFromProjectRequest.setProjectId(projectId);
        taskEndpoint.unbindTaskFromProject(taskUnbindFromProjectRequest);
        tasks = taskEndpoint.listTaskByProjectId(taskListByProjectIdRequest).getTasks();
        Assert.assertNull(tasks);
    }

}
