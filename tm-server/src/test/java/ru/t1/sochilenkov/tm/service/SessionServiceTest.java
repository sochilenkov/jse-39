package ru.t1.sochilenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.service.IConnectionService;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.api.service.ISessionService;
import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.*;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.Session;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static ru.t1.sochilenkov.tm.constant.SessionConstant.*;

@Category(UnitCategory.class)
public class SessionServiceTest {

    @NotNull
    private static ISessionService sessionService;

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private List<String> userIdList = new ArrayList<>();


    @BeforeClass
    public static void changeSchema() {
        System.setProperty("database.schema", "tm-tst");
        @NotNull IPropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        sessionService = new SessionService(connectionService);
    }

    @Before
    public void init() {
        sessionList = new ArrayList<>();
        userIdList = new ArrayList<>();
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++)
            userIdList.add(UUID.randomUUID().toString());
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++) {
            @NotNull final Session session = new Session();
            session.setUserId(userIdList.get(i));
            session.setRole(Role.USUAL);
            sessionService.add(session);
            sessionList.add(session);
        }
    }

    @After
    public void closeConnection() {
        for (@NotNull final String userId : userIdList) {
            sessionService.clear(userId);
        }
    }

    @Test
    public void testClearPositive() {
        for (final String userId : userIdList) {
            Assert.assertEquals(1, sessionService.findAll(userId).size());
            sessionService.clear(userId);
            Assert.assertEquals(0, sessionService.findAll(userId).size());
        }
    }

    @Test
    public void testClearNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.clear(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.clear(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findAll(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findAll(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllPositive() {
        @NotNull List<Session> sessions;
        for (final String userId : userIdList) {
            sessions = sessionService.findAll(userId);
            Assert.assertNotNull(sessions);
            for (final Session session : sessionList) {
                if (session.getUserId().equals(userId)) {
                    Assert.assertNotNull(
                            sessions.stream()
                                    .filter(m -> session.getUserId().equals(m.getUserId()))
                                    .filter(m -> session.getId().equals(m.getId()))
                                    .findFirst()
                                    .orElse(null)
                    );
                }
            }
        }
        for (@NotNull final String userId : userIdList) {
            sessionService.clear(userId);
        };
        sessions = sessionService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(Collections.emptyList(), sessions);
    }

    @Test
    public void testAddSessionNegative() {
        @NotNull final Session session = new Session();
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.add(NULLABLE_USER_ID, session));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.add(EMPTY_USER_ID, session));
    }

    @Test
    public void testAddSessionPositive() {
        @NotNull Session session = new Session();
        session.setUserId(UUID.randomUUID().toString());
        session.setRole(Role.USUAL);
        sessionService.add(userIdList.get(0), session);
        Assert.assertEquals(2, sessionService.findAll(userIdList.get(0)).size());
        session.setId(UUID.randomUUID().toString());
        Assert.assertNotNull(sessionService.add(session));
    }

    @Test
    public void testAdd() {
        @NotNull final Session session = new Session();
        session.setUserId(userIdList.get(0));
        session.setRole(Role.USUAL);
        for (@NotNull final String userId : userIdList) {
            sessionService.clear(userId);
        };
        sessionService.add(session);
        sessionList.add(0, session);
        Assert.assertEquals(1, sessionService.findAll(userIdList.get(0)).size());
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.existsById(NULLABLE_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.existsById(EMPTY_SESSION_ID));
    }

    @Test
    public void testExistsByIdPositive() {
        Assert.assertFalse(sessionService.existsById(UUID.randomUUID().toString()));
        for (final Session session : sessionList) {
            Assert.assertTrue(sessionService.existsById(session.getId()));
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneById(NULLABLE_USER_ID, sessionList.get(0).getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneById(EMPTY_USER_ID, sessionList.get(0).getId()));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.findOneById(UUID.randomUUID().toString(), NULLABLE_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.findOneById(UUID.randomUUID().toString(), EMPTY_SESSION_ID));
    }

    @Test
    public void testFindOneByIdPositive() {
        for (final Session session : sessionList) {
            Assert.assertEquals(session.getId(), sessionService.findOneById(session.getUserId(), session.getId()).getId());
        }
    }

    @Test
    public void testFindOneByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneByIndex(NULLABLE_USER_ID, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneByIndex(EMPTY_USER_ID, 0));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(UUID.randomUUID().toString(), NULLABLE_INDEX));
    }

    @Test
    public void testFindOneByIndexPositive() {
        for (final String userId : userIdList) {
            Assert.assertEquals(sessionList.get(userIdList.indexOf(userId)).getId(), sessionService.findOneByIndex(userId, 0).getId());
        }
    }

    @Test
    public void testGetSizeNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findAll(NULLABLE_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findAll(EMPTY_USER_ID));
    }

    @Test
    public void testRemovePositive() {
        for (final Session session : sessionList) {
            sessionService.remove(session);
            Assert.assertFalse(sessionService.findAll(session.getUserId()).contains(session));
        }
        for (final String userId : userIdList)
            Assert.assertEquals(0, sessionService.findAll(userId).size());
    }

    @Test
    public void testRemoveByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.removeById(NULLABLE_USER_ID, NULLABLE_SESSION_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.removeById(EMPTY_USER_ID, NULLABLE_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(UUID.randomUUID().toString(), NULLABLE_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(UUID.randomUUID().toString(), EMPTY_SESSION_ID));
    }

    @Test
    public void testRemoveByIdPositive() {
        for (final Session session : sessionList) {
            sessionService.removeById(session.getUserId(), session.getId());
            Assert.assertFalse(sessionService.findAll(session.getUserId()).contains(session));
            Assert.assertEquals(0, sessionService.findAll(session.getUserId()).size());
        }
    }

    @Test
    public void testRemoveByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.removeByIndex(NULLABLE_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.removeByIndex(EMPTY_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(UUID.randomUUID().toString(), NULLABLE_INDEX));
    }

    @Test
    public void testRemoveByIndexPositive() {
        for (final String userId : userIdList) {
            Assert.assertEquals(1, sessionService.findAll(userId).size());
            sessionService.removeByIndex(userId, 0);
            Assert.assertEquals(0, sessionService.findAll(userId).size());
        }
    }

}
