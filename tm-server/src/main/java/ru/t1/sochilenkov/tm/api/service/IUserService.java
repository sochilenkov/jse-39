package ru.t1.sochilenkov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.model.User;

import java.util.List;

public interface IUserService {

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email) ;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role) ;

    void clear();

    void add(@Nullable User user);

    @NotNull
    List<User> findAll();

    @Nullable
    User findById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String Login);

    @Nullable
    User findByEmail(@Nullable String email);

    void remove(@Nullable User model);

    void removeById(@Nullable String id);

    void removeByLogin(@Nullable String login);

    void removeByEmail(@Nullable String email);

    void setPassword(@Nullable String id, @Nullable String password);

    void updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    Boolean isLoginExist(@Nullable String login);

    Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
